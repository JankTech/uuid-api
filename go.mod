module gitlab.com/JankTech/uuid-api

go 1.22.1

require gitlab.com/JankTech/uuid v0.0.0-20240306214730-529bbc1c5ebd

require github.com/google/uuid v1.6.0 // indirect
