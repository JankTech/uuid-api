package main

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/JankTech/uuid"
)

type uuidResponse struct {
	Canonical string `json:"canonical"`
	URN       string `json:"urn"`
	GUID      string `json:"guid"`
	Hex       string `json:"hex"`
	Base64Std string `json:"base64Std"`
	Base64URL string `json:"base64URL"`
}

// makeUUIDResponse creates a new uuidOut struct from a uuid
func makeUUIDResponse(u uuid.UUID) uuidResponse {
	return uuidResponse{
		Canonical: u.ToStringCanonical(),
		URN:       u.ToStringURN(),
		GUID:      u.ToStringGUID(),
		Hex:       u.ToStringBase16(),
		Base64Std: u.ToStringBase64Std(),
		Base64URL: u.ToStringBase64URL(),
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc(http.MethodGet+" /", homeHandler)
	mux.HandleFunc(http.MethodGet+" /{id}", idHandler)
	log.Fatal(http.ListenAndServe(":8080", mux))
}

// homeHandler redirects the root directory to /uuid4 and handles a UUID lookup
func homeHandler(w http.ResponseWriter, req *http.Request) {
	// We want the home route to redirect to the uuid4 route.
	http.Redirect(w, req, "/uuid4", http.StatusPermanentRedirect)
}

// idHandler tries to parse a potential uuid from any format and returns
// all possible formats if it was successful
func idHandler(w http.ResponseWriter, req *http.Request) {
	id := req.PathValue("id")
	if id == "uuid4" {
		uuid4Handler(w, req)
		return
	}

	// Try to parse the string into a UUID.
	u, err := uuid.Parse(id)
	if err != nil {
		errorHandler(w, http.StatusNotFound)
		return
	}

	writeJson(w, makeUUIDResponse(u))
}

// uuid4Handler creates a version 4 UUID and returns it as json in various formats.
func uuid4Handler(w http.ResponseWriter, _ *http.Request) {
	writeJson(w, makeUUIDResponse(uuid.New()))
}

func writeJson(w http.ResponseWriter, data any) {
	encoder := json.NewEncoder(w)
	w.Header().Set("Content-Type", "application/json")
	err := encoder.Encode(data)
	if err != nil {
		errorHandler(w, http.StatusInternalServerError)
		return
	}
}

// errorHandler sets the response HTTP status to a non default value.
func errorHandler(w http.ResponseWriter, status int) {
	w.WriteHeader(status)
}
